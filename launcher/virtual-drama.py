import os, sys, time, threading, webbrowser
from http.server import HTTPServer, SimpleHTTPRequestHandler

ip   = "127.0.0.1"    
port = 9999

class Handler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args, 
            directory=os.path.dirname( os.path.realpath( sys.argv[0] )),
            **kwargs
        )

def start_server():
    httpd = HTTPServer((ip, port), Handler)
    httpd.serve_forever()

def main():
    threading.Thread(target=start_server).start()
    webbrowser.open_new("http://%s:%s" % ( 
        ip, 
        port 
    ))

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            sys.exit(0)

main()