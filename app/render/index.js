/**
 *   Renders a virtual drama configuration
 */

var current_path;
var player_scene, player_general;

const viewer = new Marzipano.Viewer( 
    document.getElementById('pano'), {
    controls: {
        mouseViewMode: 'drag'
    }
});


/**
 *   Create scene object from the path of given 3D image
 */
function getScene( path )
{
    var scene = viewer.createScene({
        source: Marzipano.ImageUrlSource.fromString( path ),
        geometry: new Marzipano.EquirectGeometry([{
            tileSize: 1024, 
            size: 1024 
        }]),
        view: new Marzipano.RectilinearView(
          null, 
          Marzipano.RectilinearView.limit.traditional(
            4096, 
            100*Math.PI/180
          )
        ),
    });
   
    return scene;
}


/**
 *   Create scene object from the path of given 3D video
 */
function getVideoScene( path )
{
    const video = document.createElement('video');
    video.loop     = true;
    video.autoplay = false;
    video.src      = path;
    conf.scenes[path].video.node = video;
    conf.scenes[path].video.node.currentTime = conf.scenes[path].video.start;
    conf.scenes[path].video.node.addEventListener('timeupdate', i => {
        if(conf.scenes[path].video.node.currentTime > conf.scenes[path].video.end ){
            conf.scenes[path].video.node.currentTime = conf.scenes[path].video.start;
        }
    });
    var wrappedVideo = new NullVideoElementWrapper(video);
    var asset        = new VideoAsset();
    asset.setVideo( wrappedVideo );

    var scene = viewer.createScene({ 
        source        : new Marzipano.SingleAssetSource(asset), 
        geometry      : new Marzipano.EquirectGeometry([{
            width: 1 
        }]), 
        view          : new Marzipano.RectilinearView(
            null, 
            Marzipano.RectilinearView.limit.traditional(
               4096, 
               100*Math.PI/180
           )
        ), 
        pinFirstLevel : false 
    });
    
    return scene;
}


/**
 *   Puts a hotspot in a scene 
 */
const putHotspot = (scene, kind, where, id, ...args) => 
{
    let hotspot = document.createElement('div');
    hotspot.id  = `hotspot--${ Date.now() }`;
    hotspot.classList.add('hotspot');
    switch(kind){
        case 'link' :
            let path = args[0];
            hotspot.classList.add('hotspot--link');
            let icon =  document.createElement('img');
            icon.src = "arrow-up.svg";
            hotspot.appendChild(icon);
            hotspot.addEventListener('click', () => {
                // Recreate videos each time we visit them
                // to avoid problems related with the video container apparently
                if( path.endsWith('.mp4') ){
                    conf.scenes[path].scene = getVideoScene(path);
                    conf.scenes[path].hotspots.forEach( hotspot => {
                        putHotspot( 
                            conf.scenes[path].scene, 
                            hotspot.kind, 
                            hotspot.coords, 
                            hotspot.id, 
                            hotspot.arg
                        );
                    });
                }
                // Switch to selected scene
                conf.scenes[ path ].scene.switchTo();
                if( conf.scenes[ path ].initial_view ){
                    conf.scenes[ path ].scene.lookTo( conf.scenes[ path ].initial_view );
                }
                // Pause videos when exiting their scenes
                if( current_path.endsWith('.mp4') ){
                    conf.scenes[current_path].video.node.pause();
                }
                if( conf.scenes[current_path].audio ){
                    player_scene.pause();
                    player_scene.currentTime = 0;
                };
                // Play automatically assets when entering their scenes
                if( path.endsWith('.mp4') ){
                    conf.scenes[path].video.node.play();
                }
                if( conf.scenes[path].audio ){
                    var audio_source  = player_scene.querySelector('source');
                    audio_source.src  = conf.scenes[path].audio;
                    player_scene.play();
                };
                // Update current_path
                current_path = path;
            });
        break;
        case 'text' :
            hotspot.classList.add('hotspot--text');
            let text =  document.createElement('span');
            text.innerHTML = args[0];
            hotspot.appendChild(text);
        break;
        case 'video' :
            hotspot.classList.add('hotspot--video');
            let iframe    =  document.createElement('iframe');
            iframe.src    = 'https://player.vimeo.com/video/' + args[0];
            iframe.frameborder = 0;
            iframe.allowfullscreen = true;
            iframe.allow = 'autoplay; fullscreen; picture-in-picture';
            hotspot.appendChild(iframe);
        break;
        case 'other_source' :
            hotspot.classList.add('hotspot--video');
            let _iframe = document.createElement('iframe');
            _iframe.src = args[0];
            _iframe.frameborder = 0;
            _iframe.allowfullscreen = true;
            _iframe.width = 640;
            _iframe.height = 480;
            hotspot.appendChild(_iframe);
        break;
    }    
    scene.hotspotContainer().createHotspot( 
        hotspot, 
        where,
        { perspective: { radius: 560, extraTransforms: "rotateX(5deg)" } }
    );
}


document.addEventListener('DOMContentLoaded', function()
{    
    var scenes = [];
    player_scene   = document.querySelector(".audio-player--scene");
    player_general = document.querySelector(".audio-player--general");
    document.title = conf.title + " | Virtual Drama Developer";
    
    // Load scenes and its hotspots
    Object.keys( conf.scenes ).forEach( function( path, index){
        var scene = path.endsWith('.mp4') ? getVideoScene(path) : getScene(path);
        conf.scenes[ path ].hotspots.forEach( hotspot => {
            putHotspot( 
                scene, 
                hotspot.kind, 
                hotspot.coords, 
                hotspot.id, 
                hotspot.arg
            );
        });
        conf.scenes[ path ].scene = scene;
    });
    var initial_scene = conf.scenes[ conf.initial_scene ].scene;

    initial_scene.switchTo();
    if( conf.scenes[ conf.initial_scene ].initial_view ){
        initial_scene.lookTo( conf.scenes[ conf.initial_scene ].initial_view );
    }
    
    // Set current path
    current_path = conf.initial_scene;
    
    // Add audios
    var audio_source;
    if( conf.audio ){
        //console.log("Añadiendo audio general");
        audio_source = document.createElement('source');
        audio_source.src  = conf.audio;
        player_general.appendChild(audio_source);
    }
    if( conf.scenes[current_path].audio ){
        //console.log("Añadiendo audio de escena");
        audio_source = document.createElement('source');
        audio_source.src = conf.scenes[current_path].audio;
        player_scene.appendChild(audio_source);
    }
    
    // Add landing interaction
    document.querySelector('.landing__button').addEventListener('click', function(){
        document.querySelector('.landing').remove();
        // Add main audio if defined
        if( conf.audio ){
            player_general.play();
        }
        // Play scene audio if defined
        if(conf.scenes[current_path].audio){
            audio_source = player_scene.querySelector('source');
            audio_source.src = conf.scenes[current_path].audio;
            player_scene.play();
        };
        // Adjust video scene in case is initial
        if( conf.initial_scene.endsWith(".mp4") ){
            conf.scenes[ conf.initial_scene ].video.node.play();
        }
    });
});