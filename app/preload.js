/**
 * Make IPC renderer available to default controller
 */

const { contextBridge, ipcRenderer, dialog } = require("electron");
const dragDrop = require( 'drag-drop' );
const io       = require('./js/io');

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object

contextBridge.exposeInMainWorld('ipc_renderer', 
{
    once   : (channel, listener) => {
        ipcRenderer.once(channel, listener);
    },
    on   : (channel, listener) => {
        ipcRenderer.on(channel, listener);
    },
    send   : ipcRenderer.send,
    invoke : ipcRenderer.invoke,
});

contextBridge.exposeInMainWorld('dragDrop', dragDrop);

contextBridge.exposeInMainWorld('io', 
{
    updateName          : io.updateConfName,
    deleteFile          : io.deleteFile,
    addFiles            : async (files) => { return io.addFiles( files ); },
    addAudioFile        : io.addAudioFile,
    getPath             : io.filepath,
    addProjectAudioFile : async (files) => { return io.addProjectAudioFile( files ); },
    removeAudio         : io.removeAudio,
    removeProjectAudio  : io.removeProjectAudio,
    setVideoInfo        : io.setVideoInfo,
    setVideoStart       : io.setVideoStart,
    setVideoEnd         : io.setVideoEnd,
    updateTitle         : io.updateTitle,
    pinScene            : io.pinScene,
    getInitialScene     : io.getInitialScene,
    createHotspot       : io.addHotspot,
    removeHotspot       : io.removeHotspot,
    getSceneData        : async (index) => { return io.getSceneData(index); },
    saveConf            : async( conf ) => { return io.saveConf(conf); },
    setInitialView      : io.setInitialView
});