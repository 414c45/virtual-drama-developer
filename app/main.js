/**
 *   Application main file
 */
 
const { app, BrowserWindow, ipcMain, dialog, Menu } = require('electron')
const path   = require('path')
const io     = require('./js/io');
const prompt = require('electron-prompt');


// For installing the app in windows
if (handleSquirrelEvent(app)) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

/**
 *    Windows
 */

function createWindow()
{
    const w = new BrowserWindow({
        webPreferences : {
            contextIsolation   : true,
            enableRemoteModule : false,
            preload            : path.join(__dirname, 'preload.js')
        },
    });
    w.webContents.session.webRequest.onHeadersReceived({ urls: [ "*://*/*" ] }, (d, c) => {
        if(d.responseHeaders['X-Frame-Options']){
            delete d.responseHeaders['X-Frame-Options'];
        } else if(d.responseHeaders['x-frame-options']) {
            delete d.responseHeaders['x-frame-options'];
        }
        c({cancel: false, responseHeaders: d.responseHeaders});
    });
    w.loadFile( path.resolve(__dirname, 'index.html') );
    return w;
}

app.whenReady().then( () =>{
    const w = createWindow();
    io.watchFiles(w);
    w.webContents.on('did-finish-load', () => { 
        io.readConf(w) 
    });
      
    app.on('activate', function(){
        if(BrowserWindow.getAllWindows().length === 0) 
        createWindow()
    });
});

/**
 *    Dialogs
 */ 
ipcMain.handle( 'dialog_add_files', ( event ) => {
    const files = dialog.showOpenDialogSync({
        properties: [ 'openFile', 'multiSelections' ],
        filters: [
            { name: 'Imágenes/Videos 3D', extensions: [ 'jpg', 'jpeg', 'insp', 'mp4' ] },
        ]
    });
    return files.map( file => {
        return {
            path : file,
            name : path.basename(file)
        }
    });
    
});

ipcMain.handle( 'dialog_add_audio_file', ( event ) => {
    return files = dialog.showOpenDialogSync({
        properties: [ 'openFile' ],
        filters: [
            { name: 'Archivos de audio', extensions: [ 'ogg', 'mp3' ] },
        ]
    });
});

/**
 *    Menus
 */
ipcMain.on('show-context-menu', e => {
    const template = [
        {
            label: 'Conectar escena',
            click: () => { 
                e.sender.send('connect_scene'); 
            }
        },
        {
            label: 'Añadir un texto', 
            click: () => { 
                prompt({
                    title: 'Añade un texto',
                    label: 'Texto a añadir',
                    inputAttrs: { type: 'text' },
                    type: 'input'
                }).then((r) => {
                    if(r){
                        e.sender.send(
                            'add_text', 
                            r
                        ); 
                    }
                }).catch(console.error);
            }
        },
        {
            label: 'Añadir un vídeo de Vimeo',
            click: () => { 
                prompt({
                    title      : 'Añade un vídeo de Vimeo',
                    label      : 'Añade la URL del video.',
                    value      : '',
                    inputAttrs : { type: 'url', placeholder: 'P. ej. https://vimeo.com/678344029' },
                    type       : 'input'
                }).then((r) => {
                    if(r) e.sender.send('add_video', r); 
                }).catch(console.error);
            }
        },
        {
            label: 'Añadir otra fuente',
            click: () => { 
                prompt({
                    title      : 'Añade una ventana flotante con otra fuente',
                    label      : 'Añade la URL de la fuente.',
                    value      : '',
                    inputAttrs : { type: 'url', placeholder: 'P. ej. https://google.es' },
                    type       : 'input'
                }).then((r) => {
                    if(r) e.sender.send('add_other_source', r); 
                }).catch(console.error);
            }
        },
    ];
    const menu = Menu.buildFromTemplate(template)
    menu.popup( BrowserWindow.fromWebContents(e.sender) )
})

ipcMain.on('show-hotspot-menu', (e, id, current_scene) => {
    const template = [
        {
            label: 'Borrar',
            click: () => { 
                e.sender.send('delete_hotspot', id); 
                io.deleteHotspot( id, current_scene );
            }
        },
    ];
    const menu = Menu.buildFromTemplate(template)
    menu.popup( BrowserWindow.fromWebContents(e.sender) )
})

function handleSquirrelEvent(application) {
    if (process.argv.length === 1) {
        return false;
    }

    const ChildProcess = require('child_process');
    const path = require('path');

    const appFolder = path.resolve(process.execPath, '..');
    const rootAtomFolder = path.resolve(appFolder, '..');
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
    const exeName = path.basename(process.execPath);

    const spawn = function(command, args) {
        let spawnedProcess, error;

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {
                detached: true
            });
        } catch (error) {}

        return spawnedProcess;
    };

    const spawnUpdate = function(args) {
        return spawn(updateDotExe, args);
    };

    const squirrelEvent = process.argv[1];
    switch (squirrelEvent) {
        case '--squirrel-install':
            return true;
        case '--squirrel-updated':
            // Optionally do things such as:
            // - Add your .exe to the PATH
            // - Write to the registry for things like file associations and
            //   explorer context menus

            // Install desktop and start menu shortcuts
            spawnUpdate(['--createShortcut', exeName]);

            setTimeout(application.quit, 1000);
            return true;

        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers

            // Remove desktop and start menu shortcuts
            spawnUpdate(['--removeShortcut', exeName]);

            setTimeout(application.quit, 1000);
            return true;

        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated

            application.quit();
            return true;
    }
};