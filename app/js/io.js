const path       = require( 'path' );
const fs         = require( 'fs-extra' );
const os         = require( 'os' );
const open       = require( 'open' );
const chokidar   = require( 'chokidar' );
const mime       = require( 'mime-types' );
const slugify    = require( 'slugify' );

// get application directory
const app_folder = path.resolve( os.homedir(), 'virtual-drama-developer' );
const renders_folder = path.resolve( os.homedir(), 'virtual-drama-developer/renders' );
const conf_file  = path.resolve( app_folder, 'conf.json' );


// read conf file
exports.readConf = ( win ) => 
{
    let conf = {};
    if( fs.existsSync(conf_file) ){
        console.log('Read existing configuration file');
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                win.webContents.send('initial_settings', conf);
            }
        });
    } else {
        console.log("Create a new configuration file");
        try {
            conf = JSON.stringify({
                title : 'My project',
                scenes : {},
                initial_scene: null
            });
            fs.writeFileSync(conf_file, conf);
            win.webContents.send('initial_settings', conf);
        } catch(err) {
            return console.log(err);
        }
    }
};

// get the list of files
exports.getFiles = () => {
    const files = fs.readdirSync( app_folder );
    return files.map( filename => {
        const file_path = path.resolve( app_folder, filename );
        const fileStats = fs.statSync( file_path );
        return {
            name: filename,
            path: file_path,
            size: Number( fileStats.size / 1000 ).toFixed( 1 ), // kb
        };
    });
};

// add files
exports.addFiles = async ( files = [] ) => {
    // ensure `app_folder` exists
    fs.ensureDirSync( app_folder );
    // copy `files` recursively (ignore duplicate file names)
    let data = {
        items: [],
        notifications: [],
    };
    files.forEach( file => 
    {
        const destination = path.resolve( app_folder, file.name );
        // Check if file already exists
        if( fs.existsSync(destination) ){
            data.notifications.push(`El archivo '${file.name}' ya existe. Renómbralo si quieres subirlo de nuevo. `);
            return;
        }
        fs.copyFileSync( file.path, destination );
        data.items.push({
            name         : file.name,
            path         : destination,
            initial_view : { yaw : 0, pitch : 0 },
            hotspots     : [],
            audio        : null,
            video        : null,
            video_muted : false,
        });
    });
    addSceneToConf( data.items );
    return data;
};

exports.updateTitle = ( new_title ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.title = new_title;
                fs.writeFileSync(conf_file, JSON.stringify(conf)); 
            }
        });
    }
};

// add audio files
exports.addAudioFile = async ( scene, files ) => {
    let result = 'ok';
    // ensure `app_folder` exists
    fs.ensureDirSync( app_folder );
    const file = files[0];    
    const destination = exports.filepath(files)
    if( !fs.existsSync(destination) ){
        fs.copyFileSync( file, destination );
    }
    if( fs.existsSync(conf_file) ){
      fs.readFile(conf_file, 'utf8', (err, data) => {
        if(err) {
            console.log(`Error reading file from disk: ${err}`);
        } else {
            const conf = JSON.parse(data);
            conf.scenes[scene].audio = destination;
            fs.writeFileSync(conf_file, JSON.stringify(conf));
        }
      });
    }
    return result;
}

// Get path of a given file
exports.filepath = (files) => {
    const file = files[0];    
    const name = path.basename(file);
    const destination = path.resolve( app_folder, name );
    return destination;
}

// add audio files to project
exports.addProjectAudioFile = async ( files ) => {
    let result = 'ok';
    // ensure `app_folder` exists
    fs.ensureDirSync( app_folder );
    const file = files[0];    
    const name = path.basename(file);
    const destination = path.resolve( app_folder, name );
    if( !fs.existsSync(destination) ){
        fs.copyFileSync( file, destination );
    }
    if( fs.existsSync(conf_file) ){
      fs.readFile(conf_file, 'utf8', (err, data) => {
        if(err) {
          console.log(`Error reading file from disk: ${err}`);
        } else {
          const conf = JSON.parse(data);
          conf.audio = destination;
          fs.writeFileSync(conf_file, JSON.stringify(conf));
        }
      });
    }
    return result;
}

// remove project audio
exports.removeProjectAudio = async ( scene ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        console.log('Deleting audio file from conf');
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                const file_path = conf.audio;
                conf.audio = null;
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                // remove file from the file system
                if( fs.existsSync( file_path ) ) {
                  fs.removeSync( file_path );
                } 
            }
        });
    }
}

// remove audio files
exports.removeAudio = async ( scene ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        console.log('Deleting audio file from conf');
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                const file_path = conf.scenes[scene].audio;
                conf.scenes[scene].audio = null;
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                // remove file from the file system
                if( fs.existsSync( file_path ) ) {
                  fs.removeSync( file_path );
                } 
            }
        });
    }
}

// set video info
exports.setVideoInfo = ( scene, video_info ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.scenes[scene].video = video_info;
                fs.writeFileSync(conf_file, JSON.stringify(conf)); 
            }
        });
    }
}

// set video info
exports.setVideoStart = ( scene, video_start ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.scenes[scene].video.start = video_start;
                fs.writeFileSync(conf_file, JSON.stringify(conf)); 
            }
        });
    }
}

// set video info
exports.setVideoEnd = ( scene, video_end ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.scenes[scene].video.end = video_end;
                fs.writeFileSync(conf_file, JSON.stringify(conf)); 
            }
        });
    }
}

// remove audio files
exports.setInitialView = ( scene, view_params ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.scenes[scene].initial_view = view_params;
                fs.writeFile(conf_file, JSON.stringify(conf)); 
            }
        });
    }
}

// set initial scene
exports.pinScene = ( scene ) => {
    // Update configuration
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.initial_scene = scene;
                fs.writeFile(conf_file, JSON.stringify(conf)); 
            }
        });
    }
}

// delete a file
exports.deleteFile = ( filename ) => {
    const file_path = path.resolve( app_folder, filename );

    // remove file from the file system
    if( fs.existsSync( file_path ) ) {
        fs.removeSync( file_path );
    }
    
    // Update configuration
    if( fs.existsSync(conf_file) ){
        console.log('Deleting file related scene from conf');
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                let conf = JSON.parse(data);
                let { [filename] : _, ...new_conf } = conf.scenes;
                conf.scenes = new_conf;
                if( conf.scenes.length == 0 ||  conf.initial_scene == filename){
                    conf.initial_scene = '';
                }
                // Delete hotspots
                Object.keys(conf.scenes).forEach( scene => {
                    conf.scenes[scene].hotspots.forEach( (hotspot, i) => {
                        if( hotspot.kind == 'link' && hotspot.arg == file_path){
                            conf.scenes[scene].hotspots[i] = undefined;
                        }
                    });
                } );
                fs.writeFileSync(conf_file, JSON.stringify(conf));
            }
        });
    } 
};

// open a file
exports.openFile = ( filename ) => {
    const file_path = path.resolve( app_folder, filename );

    // open a file using default application
    if( fs.existsSync( file_path ) ) {
        open( file_path );
    }
};

// read conf file
exports.updateConfName = ( path, name ) => 
{
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if(err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.scenes[path].name = name;
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                return conf;
            }
        });
    }
};

exports.addHotspot = (scene, kind, coords, id, arg) => {
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                conf.scenes[scene].hotspots.push({
                    kind   : kind,
                    coords : coords,
                    arg    : arg,
                    id     : id,
                });
                // TODO: buscar todos los enlaces a la escena y eliminarlos
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                return conf;
            }
        });
    }
};

exports.removeHotspot = (scene, id) => {
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                let i = conf.scenes[scene].hotspots.findIndex( i => i.id == id );
                conf.scenes[scene].hotspots.splice(i, 1);
                // TODO: buscar todos los enlaces a la escena y eliminarlos
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                return conf;
            }
        });
    }
};

exports.deleteHotspot = (id, scene) => {
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf  = JSON.parse(data);
                var hid = conf.scenes[scene].hotspots.findIndex( 
                    i => i.id == id
                );
                conf.scenes[scene].hotspots.splice(hid);
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                return conf;
            }
        });
    }
};

exports.getSceneData = async (index) => {
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                return conf.scenes[index];
            }
        });
    }
};

// Add scene to conf file
addSceneToConf = ( scene_data ) => 
{
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                scene_data.forEach( i => {
                    conf['scenes'][i.path] = i;
                } );
                fs.writeFileSync(conf_file, JSON.stringify(conf));
                return conf;
            }
        });
    }
};

exports.saveConf = ( conf ) => 
{
    if( fs.existsSync(conf_file) ){
        fs.readFile(conf_file, 'utf8', (err, data) => {
            if (err) {
                console.log(`Error reading file from disk: ${err}`);
            } else {
                const conf = JSON.parse(data);
                var destination_folder = path.resolve(
                    renders_folder, 
                    slugify( conf.title ) 
                );
                fs.ensureDirSync( destination_folder );            
                var destination_js_folder = path.resolve(
                    renders_folder, 
                    slugify( conf.title ) + '/js' 
                ); 
                fs.ensureDirSync( destination_js_folder );
                // Add needed files to render scene
                var deps = [
                    '../render/index.html',
                    '../render/style.css',
                    '../render/index.js',
                    '../render/favicon.ico',
                    '../assets/img/marca--virtual-drama.png',
                    '../assets/img/feather/arrow-up.svg',
                    'marzipano.js',
                    'NullVideoElementWrapper.js',
                    'VideoAsset.js'
                ];
                switch( os.platform() ){
                    case 'linux':
                        deps.push( '../../launcher/dist/ubuntu/virtual-drama' );
                    break;
                    case 'darwin':
                        deps.push( '../../launcher/dist/mac/virtual-drama' );
                    break;
                    case 'win32':
                        deps.push( '../../launcher/dist/windows/virtual-drama.exe' );
                    break;
                }
                deps.forEach( (dep) => {
                    var folder = dep.endsWith('.js') ? destination_js_folder : destination_folder;
                    var destination = path.resolve( folder, path.basename(dep) );
                    if( !fs.existsSync(destination)){
                        var source = path.resolve( __dirname, dep );
                        fs.copyFile( source, destination );
                    }
                })
              
                // Update paths and copy images/videos
                var assets_folder = path.resolve( destination_folder, 'assets');
                fs.ensureDirSync( assets_folder );    
                const scenes     = conf.scenes;
                const scene_paths = Object.keys(scenes);        
                scene_paths.forEach( scene_path => {
                    // Add scene image/video to assets folder
                    let filename = path.basename(scene_path);
                    let dest     = path.join( assets_folder, filename );
                    if( !fs.existsSync(dest)){
                        fs.copyFile( scene_path, dest );
                    }
                    // Add audio file to assets folder
                    const audio = scenes[scene_path].audio;
                    if(audio){
                        audio_filename = path.basename( audio );
                        dest = path.join( assets_folder, audio_filename );
                        if( !fs.existsSync(dest) ){
                            console.log('Copiando ' + audio + ' a ' + dest);
                            fs.copyFile( audio, dest );
                        }
                        conf.scenes[scene_path].audio = '/assets/' + audio_filename;
                    }
                    // Update paths
                    const new_path = '../assets/' + filename;
                    scenes[ new_path ]      = scenes[scene_path];
                    scenes[ new_path ].path = new_path;
                    scenes[ scene_path ]    = undefined;
                    // Update hotspost paths
                    scenes[ new_path ].hotspots.forEach( hotspot => {
                        if( hotspot.kind == 'link'){
                            filename = path.basename( hotspot.arg );
                            hotspot.arg = '../assets/' +  filename;
                        }
                    });
                });

                // Add general audio
                audio = conf.audio;
                if(audio){
                    filename = path.basename( audio );
                    dest = path.join( assets_folder, filename );
                    console.log('Copiando ' + audio + ' a ' + dest);
                    fs.copyFile( audio, dest );
                    conf.audio = '/assets/' + filename; 
                }
                // Update initial scene
                if( conf.initial_scene ){
                    conf.initial_scene = '../assets/' + path.basename( conf.initial_scene );
                } else {
                    let filename = path.basename( scene_paths[0] );
                    conf.initial_scene = '../assets/' + filename ; 
                }

                // Overwrite conf file 
                var conf_dest = path.resolve( destination_js_folder, 'conf.js' );
                fs.writeFile( conf_dest, 'const conf = ' + JSON.stringify(conf) );
                return destination_folder;
            }
        });
    }
};

// watch files from the application's storage directory
exports.watchFiles = ( win ) => {
    chokidar.watch( app_folder ).on( 'unlink', ( filepath ) => {
        win.webContents.send( 'app:delete-file', path.parse( filepath ).base );
    } );
}