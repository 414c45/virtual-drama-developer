

/**
 *   Renderer scripts
 */

let conf = {};
let current_scene;

// DOM elements
const project_title        = document.querySelector('.project-title');
const gallery              = document.querySelector('.view-scenes');
const gallery_items        = document.querySelector('.view-scenes__item');
const pano                 = document.getElementById('scene');
const pano_container       = document.querySelector('.app-container__right');
const audio_player         = document.querySelector('.audio-player audio');
const project_audio        = document.querySelector('.project-audio');
const project_audio_player = document.querySelector('.project-audio audio');
var button_fileadd         = document.querySelector('[data-action="add-file"]');
var button_save            = document.querySelector('[data-action="save"]');
var button_addaudio        = document.querySelector('[data-action="add-audio"]');
var button_removeaudio     = document.querySelector('[data-action="remove-audio"]');

// Global variables
let scene_index = null;
let coords = [];
let selecting_scene = false;
let hotspot = null;

/**
 *  Set up viewer
 */

const viewer = new Marzipano.Viewer( pano, {
   controls: {
       mouseViewMode: 'drag'
   }
});

/**
 *  Render scenes and add interaction to them
 */

window.ipc_renderer.on('initial_settings', (e, initial_conf) => {
    conf = initial_conf;
    project_title.innerHTML = conf.title ? conf.title : 'Sin título';
    if(initial_conf && Object.keys( initial_conf.scenes ).length > 0){
        gallery.classList.add('active');
        Object.values( initial_conf.scenes ).forEach( s => render_scene(s) );
        if(conf.audio) {
            button_addaudio.classList.add('active');
            project_audio.classList.remove('hidden');
            project_audio_player.src = conf.audio;
        }
    }
});

project_title.addEventListener('blur', (e)=>{
    const new_title = e.target.innerHTML;
    window.io.updateTitle( new_title );
    conf.title = new_title;
});

const render_scene = ( scene ) =>
{
    const path = scene.path;

    // Load template
    const item_template = document.querySelector("#scene-item");
    const new_scene     = document.importNode(item_template.content, true);

    // Set source
    const image         = new_scene.querySelector('.view-scenes__item-image');
    const video         = new_scene.querySelector('.view-scenes__item-video');
    const action_delete = new_scene.querySelector('.view-scenes__item-action--delete');
    const action_audio  = new_scene.querySelector('.view-scenes__item-action--audio');
    const action_pin    = new_scene.querySelector('.view-scenes__item-action--pin');

    if( scene.audio ){
        action_audio.classList.add('active');
    }
    if( conf.initial_scene == path ){
        action_pin.classList.add('active');
    }
    let action_open = image;
    if( path.endsWith('.mp4') ){
        video.src         = path;
        video.currentTime = 1;
        action_open = video;
        scene.scene = add_videoscene( path );
        image.remove();
    } else {
        image.src = path;
        scene.scene = add_scene( path );
        video.remove();
    }

    // Open scene in the pano
    //
    action_open.addEventListener('click', (e) => {
        e.preventDefault();
        if(selecting_scene){
            add_hotspot(viewer.scene(), 'link', coords, path);
            // Highlight scene
            pano.classList.remove('faded');
            // Remove tmp elements
            hotspot = null;
            coords  = null;
            selecting_scene = false;
        } else if(current_scene != path){
            pano.classList.add('active');
            // If current scene is playing a video pause it
            if(
                current_scene &&
                conf.scenes[current_scene].video &&
                !conf.scenes[current_scene].video.node.paused
            ) {
                conf.scenes[current_scene].video.node.pause();
            }
            // Set current scene to selected path
            current_scene = path;
            if( path.endsWith('.mp4') )
            {
                conf.scenes[current_scene].scene = add_videoscene( path );
            }
            conf.scenes[path].scene.switchTo();
            if('initial_view' in conf.scenes[path]){
                conf.scenes[path].scene.lookTo( conf.scenes[path].initial_view, {
                    transitionDuration: 0
                });
            }
            // Add existing audio
            if( !audio_player.paused ) {
                audio_player.pause();
                audio_player.currentTime = 0;
            }
            if(scene.audio){
                pano_container.classList.add('has-audio');
                const audio_source = document.createElement('source');
                audio_source.src  = scene.audio;
                audio_source.type = 'audio/mp3';
                audio_player.appendChild(audio_source);
            } else {
                pano_container.classList.remove('has-audio');
                const audio_source = audio_player.querySelector('source');
                if(audio_source) audio_source.remove();
            }
            // Video
            if(scene.video && scene.video.node){
                scene.video.node.currentTime = scene.video.start;
                scene.video.node.src  = path;
                scene.video.node.pause();
                pano_container.classList.add('has-video');
            } else {
                pano_container.classList.remove('has-video');
            }
        }
    });

    // Add audio to scene
    //
    action_audio.addEventListener('click', (e) => {
        window.ipc_renderer.invoke( 'dialog_add_audio_file' ).then( files => {
            addAudioFile( path, files );
            const audio = window.io.getPath( files );
            action_audio.classList.add('active');
            pano_container.classList.add('has-audio');
            const audio_source = document.createElement('source');
            audio_source.src  = audio;
            audio_source.type = 'audio/mp3';
            audio_player.appendChild(audio_source);
        });
    });

    // Delete scene
    //
    action_delete.addEventListener('click', (e) =>
    {
        window.io.deleteFile( path );
        delete conf.scenes[path];
        Object.keys( conf.scenes ).forEach( scene => {
            conf.scenes[scene].hotspots.forEach( (hotspot, i) => {
                if( hotspot.kind == 'link' && hotspot.arg == path){
                    conf.scenes[scene].hotspots[i] = undefined;
                }
            });
        });
        action_delete.parentNode.parentNode.parentNode.remove();
        var remaining = document.querySelectorAll('.view-scenes__item').length;
        // delete hotspots linking to this scene
        var hotsposts_pointing = document.querySelectorAll(`[data-link='${path}']`);
        hotsposts_pointing.forEach( i => i.remove() );
        if( path == conf.initial_scene || remaining == 0) {
            var new_initial_scene =
            conf.initial_scene = '';
            let active = document.querySelector('.view-scenes__item-action--pin.active');
            if(active) active.classList.remove('active');
            if( remaining == 0 ){
                document.querySelector('#scene').classList.remove('active');
                document.querySelector('#uploader').classList.remove('active');
            }
        }
    });

    // Pin scene as initial
    action_pin.addEventListener('click', (e) => {
        window.io.pinScene( path );
        conf.initial_scene = path;
        var active = document.querySelector('.view-scenes__item-action--pin.active');
        if(active) active.classList.remove('active');
        action_pin.classList.add('active');
        new Notification("Escena editada", {
            body: `Has establecido la escena como inicial éxitosamente`
        });
    });

    // Set scene name
    const name = new_scene.querySelector('.view-scenes__item-name');
    name.innerHTML = scene.name;
    name.addEventListener('blur', (e)=>{
        const new_name = e.target.innerHTML;
        window.io.updateName( path, new_name );
        conf.scenes[path].name = new_name;
    });
    // Append element to gallery
    gallery.appendChild( new_scene );
}

/**
 *  Set scene
 */

add_scene = (path) =>
{
    const scene = viewer.createScene({
        source: Marzipano.ImageUrlSource.fromString( path ),
        geometry: new Marzipano.EquirectGeometry([{
          tileSize: 1024,
          size: 1024
        }]),
        view: new Marzipano.RectilinearView(
          null,
          Marzipano.RectilinearView.limit.traditional(
            4096,
            100*Math.PI/180
          )
        ),
    });
    conf.scenes[ path ].hotspots.forEach( hotspot => {
        if(hotspot){
            put_hotspot(scene, hotspot.kind, hotspot.coords, hotspot.id, hotspot.arg);
        }
    });
    return scene;
};

add_videoscene = (path) =>
{
    const video = document.createElement('video');
    video.autoplay = true;
    video.loop     = true;
    video.muted    = false;
    video.src      = path;
    video.pause();
    var wrappedVideo = new NullVideoElementWrapper(video);
    var asset        = new VideoAsset();
    asset.setVideo( wrappedVideo );

    var scene_video = viewer.createScene({
        source        : new Marzipano.SingleAssetSource(asset),
        geometry      : new Marzipano.EquirectGeometry([{
            width: 1
        }]),
        view          : new Marzipano.RectilinearView(
            null,
            Marzipano.RectilinearView.limit.traditional(
               4096,
               100*Math.PI/180
           )
        ),
        pinFirstLevel : false
    });
    if( !conf.scenes[path].video ){
        var i = setInterval(function() {
          	if(video.readyState > 0) {
                const video_info = {
                    duration : video.duration,
                    start    : 0,
                    end      : video.duration
                };
                window.io.setVideoInfo( path, video_info );
                video_info.node = video;
                conf.scenes[path].video = video_info;
                conf.scenes[path].video.node.currentTime = conf.scenes[path].video.start;
                conf.scenes[path].video.node.addEventListener('timeupdate', i => {
                    if(conf.scenes[path].video.node.currentTime > conf.scenes[path].video.end ){
                        conf.scenes[path].video.node.currentTime = conf.scenes[path].video.start;
                    }
                });
          	}
        }, 200);
    } else {
        conf.scenes[path].video.node = video;
        conf.scenes[path].video.node.currentTime = conf.scenes[path].video.start;
        conf.scenes[path].video.node.addEventListener('timeupdate', i => {
            if(conf.scenes[path].video.node.currentTime > conf.scenes[path].video.end ){
                conf.scenes[path].video.node.currentTime = conf.scenes[path].video.start;
            }
        });
    }
    return scene_video;
};

/**
 *  Upload files via drag&drop and dialog
 */

addFiles = (filedata) =>
{
    const files = filedata.map( file => { return {
        name: file.name,
        path: file.path
    }} );
    window.io.addFiles( files ).then( data => {
        if(data.notifications.length == 0){
            new Notification("Archivos subidos con éxito", {
                body: `Has añadido ${ data.items.length } archivos`
            });
            data.items.forEach( i => {
                conf.scenes[ i.path ] = {
                    name     : i.name,
                    path     : i.path,
                    hotspots : [],
                    scene    : null,
                };
                render_scene( conf.scenes[ i.path ] );
            });
            if(!gallery.classList.contains('active'))
                gallery.classList.add('active');;
        } else {
            new Notification("No se han podido subir todos los archivos", {
              body: data.notifications.join()
            });
        }
    });
};

addAudioFile = (scene_path, file) =>
{
    window.io.addAudioFile( scene_path, file ).then( result => {
        if( result == 'ok' ){
            new Notification("Archivos subidos con éxito", {
                body: `Has añadido el archivo de audio a la escena`
            });
            conf.scenes[ scene_path ].audio = file;
        } else {
            new Notification("No se ha podido subir el archivo", {
              body: 'Ha habido problemas subiendo el archivo'
            });
        }
    });
};

addProjectAudioFile = (file) =>
{
    window.io.addProjectAudioFile( file ).then( result => {
        if( result == 'ok' ){
            new Notification("Archivos subidos con éxito", {
                body: `Has añadido un archivo general de audio al proyecto`
            });
            conf.audio = file;
            project_audio_player.src = conf.audio;
        } else {
            new Notification("No se ha podido subir el archivo", {
              body: 'Ha habido problemas subiendo el archivo'
            });
        }
    });
};


/**
 *  Drag&Drop
 */

dragDrop('#uploader', files =>
{
    addFiles( files );
});

/**
 *  Proyect actions
 */

button_fileadd.addEventListener('click', () => {
    window.ipc_renderer.invoke( 'dialog_add_files' ).then( files => {
        addFiles( files );
    });
});

button_save.addEventListener('click', (e) => {
    window.io.saveConf(conf).then( destination_folder => {
        new Notification("Has exportado el proyecto en HTML con éxito", {
          body: `El proyecto está en la carpeta virtual-drama-developer de tu carpeta personal`
        });
    });
});

button_addaudio.addEventListener('click', (e) => {
    window.ipc_renderer.invoke( 'dialog_add_audio_file' ).then( files => {
        addProjectAudioFile( files );
        button_addaudio.classList.add('active');
        project_audio.classList.remove('hidden');
    });
});

button_removeaudio.addEventListener('click', (e) => {
    window.io.removeProjectAudio();
    button_addaudio.classList.remove('active');
    project_audio.classList.add('hidden');
    conf.audio = null;
});

/**
 *  Hotspots
 */

/**
 *  Add hotspot
 */

const add_hotspot = (scene, kind, where, ...args) =>
{
    // Add hotspot to DOM
    // Add creation timestamp as ID, so we can identify hotspots and manipulate them in DOM, for instance
    const id = Date.now();
    put_hotspot( scene, kind, where, id, ...args );
    // Add hotspot to conf variable
    conf.scenes[ current_scene ].hotspots.push({
        kind   : kind,
        coords : coords,
        arg    : args[0],
        id     : id,
    });
    // Add hotspot to conf file
    window.io.createHotspot(current_scene, kind, where, id, args[0]);
}

const remove_hotspot = (id) =>
{
    document.querySelector(`#hotspot--${id}`).remove();
    let i = conf.scenes[current_scene].hotspots.findIndex( i => i.id == id );
    conf.scenes[current_scene].hotspots.splice(i, 1);
    window.io.removeHotspot(current_scene, id);
}

/**
 *  Put hotspot in the DOM
 */

const put_hotspot = (scene, kind, where, id, ...args) =>
{
    let hotspot = document.createElement('div');
    hotspot.id  = `hotspot--${ id }`;
    hotspot.classList.add('hotspot');
    switch(kind){
        case 'link' :
        hotspot.classList.add('hotspot--link');
        let icon =  document.createElement('img');
        icon.src = "assets/img/feather/arrow-up.svg";
        icon.dataset.link = args[0];
        hotspot.appendChild(icon);
        hotspot.addEventListener('click', () => {
            var sc = conf.scenes[ args[0] ];
            sc.scene.switchTo();
            sc.scene.lookTo( sc.initial_view, {
                transitionDuration: 0
            });
            current_scene = args[0];
        });
        break;
        case 'text' :
        hotspot.classList.add('hotspot--text');
        let text =  document.createElement('span');
        text.innerHTML = args[0];
        hotspot.appendChild(text);
        break;
        case 'video' :
        hotspot.classList.add('hotspot--video');
        let iframe    =  document.createElement('iframe');
        iframe.src    = 'https://player.vimeo.com/video/' + args[0];
        iframe.frameborder = 0;
        iframe.allowfullscreen = true;
        iframe.allow = 'autoplay; fullscreen; picture-in-picture';
        iframe.width = 640;
        iframe.height = 480;
        hotspot.appendChild(iframe);
        let close = document.createElement('span');
        close.innerHTML = '☓';
        close.classList.add('hotspot__close--video');
        close.addEventListener('click', function(){
            remove_hotspot( id );
        });
        hotspot.appendChild(close);
        break;
        case 'other_source' :
        hotspot.classList.add('hotspot--video');
        let _iframe =  document.createElement('iframe');
        _iframe.src    = args[0];
        _iframe.frameborder = 0;
        _iframe.allowfullscreen = true;
        _iframe.width = 640;
        _iframe.height = 480;
        _iframe.allow = 'autoplay; fullscreen; picture-in-picture';
        hotspot.appendChild(_iframe);
        let _close = document.createElement('span');
        _close.innerHTML = '☓';
        _close.classList.add('hotspot__close--video');
        _close.addEventListener('click', function(){
            remove_hotspot( id );
        });
        hotspot.appendChild(_close);
        break;
    }
    scene.hotspotContainer().createHotspot(
        hotspot,
        where,
        { perspective: { radius: 560, extraTransforms: "rotateX(5deg)" } }
    );
}

/**
 *  Scene actions
 */

document.querySelector('.scene-actions__item--set-view').addEventListener('click', e => {
    const view = viewer.scene().view();
    const pano_position = pano.getBoundingClientRect();
    const view_coords = viewer.view().screenToCoordinates({
        x: pano_position.left + pano_position.width/2 - gallery.offsetWidth,
        y: pano_position.top  + pano_position.height/2
    });
    view_coords.fov = view.fov();
    window.io.setInitialView( current_scene, view_coords );
    conf.scenes[ current_scene ].initial_view = view_coords;
    new Notification("Vista inicial cambiada", {
        body: `Has situado la vista inicial de la escena con éxito.`
    });
});


/**
 *  Audio actions 
 */

document.querySelector('.audio-player__action--remove').addEventListener('click', e => {
    window.io.removeAudio(current_scene);
    pano_container.classList.remove('has-audio');
    const view_item = document.querySelector('img[src="' + current_scene + '"]').parentNode;
    view_item.querySelector('.view-scenes__item-action--audio').classList.remove('active');
    if( !audio_player.paused ) audio_player.pause();
    conf.scenes[current_scene].audio = "";
    audio_player.querySelector('source').setAttribute("src", "");
});

/**
 *  Video actions
 */

document.querySelector('.video-controls__action--play').addEventListener('click',  e => {
    conf.scenes[current_scene].video.node.play();
});
document.querySelector('.video-controls__action--pause').addEventListener('click', e => {
    conf.scenes[current_scene].video.node.pause();
});
document.querySelector('.video-controls__action--start').addEventListener('click', e => {
    const time = conf.scenes[current_scene].video.node.currentTime;
    window.io.setVideoStart(current_scene, time);
    conf.scenes[current_scene].video.start = time;
    new Notification("Vídeo modificado", {
        body: `Has cambiado el inicio del video éxitosamente.`
    });
});
document.querySelector('.video-controls__action--end').addEventListener('click', e => {
    const time = conf.scenes[current_scene].video.node.currentTime;
    window.io.setVideoEnd(current_scene, time);
    conf.scenes[current_scene].video.end = time;
    new Notification("Vídeo modificado", {
        body: `Has cambiado el final del video éxitosamente.`
    });
});


/**
 *  Launch about
 */

document.querySelector('[data-action="launch-about"]').addEventListener('click', e => {
    document.querySelector('.about-modal').classList.remove('faded');
});
document.querySelector('.about-modal__close').addEventListener('click', e => {
    document.querySelector('.about-modal').classList.add('faded');
});

/**
 *  Context menus
 */

/**
 *  General context menu logic
 *  (when user right-click the scene)
 */

window.addEventListener('contextmenu', (e) => {
    e.preventDefault();
    coords = viewer.view().screenToCoordinates({
        x: e.clientX - gallery.offsetWidth,
        y: e.clientY
    });
    if( e.target.parentNode.classList.contains('hotspot') ){
        window.ipc_renderer.send('show-hotspot-menu', e.target.parentNode.id, current_scene);
    } else {
        window.ipc_renderer.send('show-context-menu');
    }
});

/**
 *  Events related to context menu
 *  @see main.js
 */

window.ipc_renderer.on( 'connect_scene', () => {
    // Fade out scene, add cancel button
    pano.classList.add('faded');
    // Prompt to select new scene
    selecting_scene = true;
});

document.querySelector('[data-action="cancel-selection"]').addEventListener('click', i => {
    // Fade out scene, add cancel button
    pano.classList.remove('faded');
    // Prompt to select new scene
    selecting_scene = false;
});

window.ipc_renderer.on( 'delete_hotspot', (e, id) => {
    var i = id.replace('hotspot--', '');
    remove_hotspot(i);
});

window.ipc_renderer.on( 'add_text', (e, text) => {
    add_hotspot( viewer.scene(), 'text', coords, text );
});

window.ipc_renderer.on( 'add_video', (e, url) => {
    var id = url.split('/').reverse()[0];
    add_hotspot( viewer.scene(), 'video', coords, id );
});

window.ipc_renderer.on( 'add_other_source', (e, url) => {
    add_hotspot( viewer.scene(), 'other_source', coords, url );
});