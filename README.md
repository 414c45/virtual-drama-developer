# Virtual Drama Developer

Virtual Drama Developer is a desktop virtual panorama software. It's written in Electron.js, using Marzipano (https://www.marzipano.net/).

To package in the different operative systems run:

`npm run package-win`

`npm run package-linux`

`electron-builder` (for mac as electron-packager seems not to work with MAC)

And to create a debian installer run:

`npm run create-debian-installer`

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
