const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller
const path = require('path')

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error)
    process.exit(1)
  })

function getInstallerConfig () {
  console.log('creating windows installer')
  const rootPath = path.join('./')
  const outPath  = path.join(rootPath, 'release-builds')

  return Promise.resolve({
      appDirectory    : path.join(outPath, 'virtual-drama-developer-win32-ia32/'),
      authors         : 'tejido.io',
      noMsi           : true,
      outputDirectory : path.join(outPath, 'windows-installer'),
      exe             : 'virtual-drama-developer.exe',
      setupExe        : 'virtual-drama-developer-install.exe',
      setupIcon       : path.join(rootPath, 'icons', 'icono-virtual-drama.ico')
  })
}